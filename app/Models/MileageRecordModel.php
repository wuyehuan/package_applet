<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MileageRecordModel extends Model
{
    // 里程
    protected $table = 'today_mileage';
}
