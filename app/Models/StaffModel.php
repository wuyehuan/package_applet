<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class StaffModel extends User implements JWTSubject
{
    use Notifiable;

    // 绑定表
    protected $table = 'staff';


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     *
     */
    public function setPasswordAttribute($value){
        $this -> attributes['password'] = bcrypt($value);
    }

    //添加白名单
    protected $fillable = [
        'account',
        'password',
        'staff_name',
        'staff_head',
        'car_license',
        'total_mileage',
        'created_at',
        'updated_at'
    ];
}
