<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    // 绑定表
    protected $table='order';

    /**
     * 关联模型 关联商品详情
     */
    public function order_info(){
        return $this -> hasMany('App\Models\OrderInfoModel','order_id','id');
    }


    /**
     *
     */

}
