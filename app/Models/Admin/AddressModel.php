<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AddressModel extends Model
{
    // 绑定表
    protected $table = 'address';
}
