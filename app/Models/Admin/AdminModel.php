<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AdminModel extends Model
{
    // 绑定表
    protected $table = 'admin_users';
}
