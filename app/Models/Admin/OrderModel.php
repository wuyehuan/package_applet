<?php

namespace App\Models\Admin;

use App\Models\SignModel;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    // 绑定表
    protected $table = 'order';


    /**
     * 关联模型 关联员工表
     */
    public function staff(){
        return $this -> belongsTo('App\Models\Admin\StaffModel','staff_id');
    }


    /**
     * 关联模型 详情
     */
    public function info(){
        return $this -> hasMany(OrderInfoModel::class,'order_id');
    }


    /**
     * 关联模型 关联签名
     */
    public function sign(){
        return $this -> hasOne(SignModel::class,'order_id');
    }


    /**
     * 获取器
     */
    //public function getIsCollectionAttribute($value){
    //    switch ($value){
    //        case 1:
    //            return '是';
    //            break;
    //
    //        case 2:
    //            return '否';
    //            break;
    //
    //        default:
    //            return '未定义状态';
    //            break;
    //    }
    //}


    /**
     * @param $value
     * @return string
     * 获取器
     * 1已下单 2完成签收 3签收异常
     *  1运输中 2完成签收 3签收异常 4重新派单 5订单作废',
     */
    public function getOrderStatusAttribute($value){
        switch ($value){
            case 1:
                return '运输中';
                break;

            case 2:
                return '签收';
                break;

            case 3:
                return '异常';
                break;

            case 4:
                return '重新派单';
                break;

            case 5:
                return '返回收件人';
                break;

            default:
                return '未定义状态';
                break;
        }
    }

}
