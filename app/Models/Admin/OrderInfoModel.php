<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class OrderInfoModel extends Model
{
    // 绑定表
    protected $table = 'order_info';
}
