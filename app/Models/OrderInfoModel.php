<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInfoModel extends Model
{
    // 绑定表
    protected $table='order_info';

    // 设置黑名单
    protected $guarded = [];

    protected $fillable = ['goods_name', 'goods_unit','goods_number'];
}
