<?php
/**
 * Created by PhpStorm.
 * User: zhan
 * Date: 2019/9/19  15:46
 */

/**
 * 公用的方法  返回json数据，进行信息的提示
 * @param $status 状态
 * @param string $message 提示信息
 * @param array $data 返回数据
 */

/**
 *获取唯一订单号
 * @return string
 */
function order_number(){
    return date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
}


/*
  功能：判断某个目录下是否存在文件；
  参数：$path —— 要进行判断的目录，使用绝对路径
  返回值：存在 - true  不存在 - false
*/
function dir_exist_file($path) {

    if (!is_dir($path)) {
        return false;
    }

    $files = scandir($path);
    //return $files;

    // 删除  "." 和 ".."
    unset($files[0]);
    unset($files[1]);

    // 判断是否为空
    if (!empty($files[2])) {
        return true;
    }

    return false;
}


// 获取当前域名
function getDomain(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
}

