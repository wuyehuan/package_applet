<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    // $router->get('/', 'HomeController@index')->name('admin.home');
    $router->get('/', function(){
        return redirect('admin/order');
    });

    // 员工
    $router->resource('staff', StaffController::class);

    // 订单
    $router->resource('order', OrderController::class);

    // 显示地图
    $router->get('board/map', 'BoardController@map');
    // 获取订单列表
    $router->get('board/order', 'BoardController@orderList');
    // 添加地址
    // $router->get('address', AddressController::class);
    // 获取省份
    $router -> get('address/province','AddressController@province');
    // 获取市
    $router -> get('address/city','AddressController@city');

    // 大数据
    $router -> resource('board', BoardController::class);
    // 获取省份数据
    $router -> get('get-province','BoardController@getProvince');
    // 里程记录
    $router->resource('mileage-record', MileageRecordController::class);



});
