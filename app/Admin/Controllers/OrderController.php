<?php

namespace App\Admin\Controllers;

use App\Models\Admin\OrderModel;
use App\Models\Admin\StaffModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

class OrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '订单管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new OrderModel);
        $grid->disableCreateButton();
        $grid -> column('staff.staff_name',__('揽件员'));
        $grid->column('order_number', __('物流单号'));
        $grid
            ->column('sender-address', __('发货地址'))
            -> display(function(){
                return $this -> sender_province . $this -> sender_city . $this -> sender_district . $this -> sender_address;
            })
            -> width(230);;
        $grid->column('sender_name', __('寄件人'));
        $grid->column('sender_phone', __('寄件人电话'));

        $grid
            -> column('consignee-address','收货地址')
            -> display(function($title){
                return $this -> consignee_province . $this -> consignee_city .  $this -> consignee_district .  $this -> consignee_address;
            })
            -> width(230);
        $grid->column('consignee_name', __('收件人'));
        $grid->column('consignee_phone', __('收件人电话'));
        $grid->column('total_goods_number', __('数量'));
        $grid->column('sender_money', __('运费'));
        $grid->column('pay_type', '运费类型')->using([1 => '到付',2 => '现金',3 => '扫码']);
        $grid->column('is_collection', __('是否代收货款'))->bool(['1' => true, '2' => false]);
        $grid->column('goods_money', __('代收金额'));
        $grid->column('sign.sign_goods_type', '代收方式')->using([1 => '扫码',2 => '现金',3 => '无']);
        $grid->column('order_status', __('运输状态'));
        $grid->column('created_at', __('寄件时间'));
        // 显示多图
        $grid->column('goods_img','订单图')->display(function ($pictures) {
            return \DB::table('images') -> whereIn('id',explode(',',$pictures)) -> pluck('url');
        })->carousel(120,120,'/');

        $grid->actions(function ($actions) {

            // 去掉删除
            $actions->disableDelete();
            // 去掉编辑
            $actions->disableEdit();
            // 去掉查看
            // $actions->disableView();
        });

        // 获取当前管理员信息
        $adminId = Admin::user() -> id;
        if($adminId != 1){
            // 说明不是超级管理员 查找属于该管理员下的所有员工订单
            // $staffIds = StaffModel::where('admin_id','=',$adminId) -> pluck('id');
            // $grid -> model() -> whereIn('staff_id',$staffIds);
            $grid -> model() -> where('admin_id','=',$adminId);
        }

        $grid -> model() -> orderBy('id','desc');

        // 查询
        $grid->filter(function($filter) use($adminId){
            // 去掉默认的id过滤器  1运输中 2完成签收 3签收异常 4重新派单 5订单作废',
            if($adminId == 1){
                $staffList = \DB::table('staff') -> get();
            }else{
                $staffList = \DB::table('staff') -> where('admin_id','=',$adminId) -> get();
            }
            $staff = [];
            foreach ($staffList as $v){
                $staff[$v -> id] = $v -> staff_name;
            }
            $filter -> disableIdFilter();
            $filter -> equal('staff_id',__('揽件员')) -> select($staff);
            $filter -> equal('order_number','物流单号');
            $filter -> equal('sender_phone','寄件人电话');
            $filter -> equal('consignee_phone','收件人电话');
            $filter -> equal('order_status','运输状态') -> select(['1' => '运输中','2' => '完成签收','3' => '签收异常','4' => '重新派单','5' => '订单作废']);
            $filter -> between('created_at', '时间') -> datetime();
        });

        // 导出
        $orderExporter = new \App\Admin\Extensions\OrderExporter();
        $orderExporter -> setFileName(date('YmdHis'));
        $grid->exporter($orderExporter);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OrderModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('staff_id', __('Staff id'));
        $show->field('order_number', __('物流单号'));
        $show->field('sender_province', __('发货地址(省)'));
        $show->field('sender_city', __('发货地址(市)'));
        $show->field('sender_district', __('发货地址(区)'));
        $show->field('sender_address', __('发货地址(详细地址)'));
        $show->field('sender_name', __('寄件人'));
        $show->field('sender_phone', __('寄件人电话'));
        $show->field('consignee_province', __('收货地址(省)'));
        $show->field('consignee_city', __('收货地址(市)'));
        $show->field('consignee_district', __('收货地址(区)'));
        $show->field('consignee_address', __('收货地址(详细地址)'));
        $show->field('consignee_name', __('收件人'));
        $show->field('consignee_phone', __('收件人电话'));
        $show->field('goods_number', __('货物数量'));
        $show->field('sender_money', __('运费'));
        $show->field('pay_type', '运费类型')->using([1 => '到付',2 => '现金',3 => '扫码']);
        $show->field('is_collection', __('是否代收'))->using(['1' => '是', '2' => '不是']);;
        $show->field('goods_money', __('代收金额'));
        $show->field('', __('代收方式'))->as(function(){
            $name = \DB::table('sign') -> where('order_id','=',$this -> id) -> value('sign_goods_type');
            switch ($name){
                case 1:
                    return '扫码';
                    break;
                case 2:
                    return '现金';
                    break;
                default:
                    return '无';
                    break;
            }
        });
        // $grid->column('sign.sign_goods_type', '代收方式')->using([1 => '扫码',2 => '现金',3 => '无']);
        $show->field('order_status', __('物流状态'));
        $show->field('goods_remarks', __('货物备注'));
        $show->field('order_remarks', __('物流备注'));
        $show->field('created_at', __('下单时间'));

        $show->info('货物详情', function ($comments) {

            $comments->id();
            $comments->goods_name('货物名称');
            $comments->goods_number('数量');
            $comments->goods_unit('单位');
            $comments->created_at('添加时间');
            $comments->updated_at('修改时间');
            // 去掉按钮
            $comments->disableActions();
            $comments->disableFilter();
            $comments->disableExport();
            $comments->disableCreateButton();
        });

        $show->goods_img('图片')->as(function ($title){
            return \DB::table('images') -> whereIn('id',explode(',',$title)) -> pluck('url');
        })->carousel(320,240, '/');

        $show->sign('签名信息', function ($sign) {

            $sign->id();
            $sign->sign_mid('签收成功图片')->as(function($imgId){
                return \DB::table('images') -> where('id','=',$imgId) -> value('url');
            })->image('/',320,240);;
            $sign->record_mid('签收异常图片')->as(function ($imgId){
                return \DB::table('images') -> where('id','=',$imgId) -> value('url');
            })->image('/',320,240);
            $sign->sign_goods_type('代收方式')->using([1 => '扫码',2 => '现金', 3 => '无']);
            $sign->sign_adress('签收地址');
            $sign->updated_at('签收时间');
            // 去掉按钮
            $sign->panel()
                ->tools(function ($tools) {
                    $tools->disableEdit();
                    $tools->disableDelete();
                    $tools->disableList();
                });
        });

        $show->panel()
            ->tools(function ($tools) {
                $tools->disableEdit();
                $tools->disableDelete();
            });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new OrderModel);

        $form->number('staff_id', __('Staff id'));
        $form->text('order_number', __('Order number'));
        $form->text('sender_province', __('Sender province'));
        $form->text('sender_city', __('Sender city'));
        $form->text('sender_district', __('Sender district'));
        $form->text('sender_address', __('Sender address'));
        $form->text('sender_name', __('Sender name'));
        $form->number('sender_phone', __('Sender phone'));
        $form->text('consignee_province', __('Consignee province'));
        $form->text('consignee_city', __('Consignee city'));
        $form->text('consignee_district', __('Consignee district'));
        $form->text('consignee_address', __('Consignee address'));
        $form->text('consignee_name', __('Consignee name'));
        $form->number('consignee_phone', __('Consignee phone'));
        $form->number('goods_number', __('Goods number'))->default(1);
        $form->number('order_money', __('Order money'));
        $form->number('collection_money', __('Collection money'));
        $form->number('is_collection', __('Is collection'))->default(2);
        $form->number('order_status', __('Order status'));

        return $form;
    }
}
