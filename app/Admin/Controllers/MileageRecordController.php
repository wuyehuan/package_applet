<?php

namespace App\Admin\Controllers;

use App\Models\MileageRecordModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class MileageRecordController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '里程记录';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        $grid = new Grid(new MileageRecordModel);

        $grid->column('id', __('Id'));
        // $grid->column('staff_id', __('Staff id'));
        $grid->column('start_mid', '开始里程(图)')->display(function(){
            return \DB::table('images') -> where('id','=',$this -> start_mid) -> value('url');
        }) -> image('/',85,85);
        $grid->column('deliver_mid', '结束里程(图)')->display(function(){
            return \DB::table('images') -> where('id','=',$this -> deliver_mid) -> value('url');
        }) -> image('/',85,85);;
        $grid->column('start_mileage', '开始里程');
        $grid->column('deliver_mileage', '结束里程');
        $grid->column('total_new_mileage', '总里程');
        $grid->column('start_address', '发车地址');
        $grid->column('end_address', '交车地址');
        $grid->column('created_at', '发车时间');
        $grid->column('updated_at', '交车时间');

        $grid -> model() -> where('staff_id','=',request() -> input('staff_id'));

        // 全部关闭
        $grid->disableActions();
        $grid->disableCreateButton();
        $grid->disableExport();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MileageRecordModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('staff_id', __('Staff id'));
        $show->field('start_mid', __('Start mid'));
        $show->field('deliver_mid', __('Deliver mid'));
        $show->field('start_mileage', __('Start mileage'));
        $show->field('deliver_mileage', __('Deliver mileage'));
        $show->field('total_new_mileage', __('Total new mileage'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return back() -> withErrors('异常');
        $form = new Form(new MileageRecordModel);

        $form->number('staff_id', __('Staff id'));
        $form->number('start_mid', __('Start mid'));
        $form->number('deliver_mid', __('Deliver mid'));
        $form->decimal('start_mileage', __('Start mileage'));
        $form->decimal('deliver_mileage', __('Deliver mileage'))->default(0.00);
        $form->decimal('total_new_mileage', __('Total new mileage'))->default(0.00);

        return $form;
    }
}
