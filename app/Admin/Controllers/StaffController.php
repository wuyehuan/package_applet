<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Staff\Depart;
use App\Models\Admin\StaffModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

class StaffController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '员工管理';


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new StaffModel);

        $grid->column('id', __('Id'));
        $grid->column ('admin.username',__('所属片区')) -> label();
        $grid->column('account', __('账号'));
        $grid->column('phone', '手机号码');
        $grid->column('staff_name', __('员工姓名'));
        $grid->column('staff_head', __('头像')) -> image('',80,80);
        $grid->column('car_license', __('车牌号'));
        $grid->column('today_mileage', __('今日里程'));
        $grid->column('total_mileage', __('总里程数'));
        $grid->column('created_at', __('注册时间'));

        // 验证是否为超级管理员
        $admin = Admin::user();
        if($admin -> id != 1){
            $grid -> model() -> where('admin_id','=',$admin -> id);
        }
        $grid -> model() -> orderBy('id','desc');

        // 查询
        $grid -> filter(function ($filter){
            $filter -> disableIdFilter();
            $filter -> equal('phone','手机号码');
        });

        $grid -> actions(function ($action){
            $action -> add(new Depart());
        });

        // 导出
        $orderExporter = new \App\Admin\Extensions\StaffExporter();
        $orderExporter -> setFileName(date('YmdHis'));
        $grid->exporter($orderExporter);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(StaffModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('account', __('账号'));
        $show->field('phone', __('手机号码'));
        $show->field('staff_name', __('员工名称'));
        $show->staff_head(__('员工头像')) -> image();;
        $show->field('car_license', __('车牌号'));
        $show->field('today_mileage', __('今日里程'));
        $show->field('total_mileage', __('总里程'));
        $show->field('created_at', __('注册时间'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new StaffModel);

        $form->text('account', __('账号'))
            // -> rules('required|unique:staff',['required' => '账号不能为空','unique' => '该账号已存在'])
            ->creationRules(['required', "unique:staff"],['unique' => '该手机号码已存在'])
            ->updateRules(['required', "unique:staff,account,{{id}}"],['unique' => '该手机号码已存在']);;
        $form->text('phone', __('手机号码'))
            // -> rules('required|unique:staff',['required' => '手机号码不能为空','unique' => '该手机号码已存在']);
            ->creationRules(['required', "unique:staff"],['unique' => '该手机号码已存在'])
            ->updateRules(['required', "unique:staff,phone,{{id}}"],['unique' => '该手机号码已存在']);
        $form->password('password', __('密码'))
            -> rules('required|min:6',['min' => '密码最少为6位']);
        $form->text('staff_name', __('员工名称')) -> rules('required');
        $form->image('staff_head', __('员工头像')) -> move('images/' . date('Ymd'));
        $form->text('car_license', __('车牌号'));

        $form->saving(function (Form $form) {

            if ($form->password && $form->model()->password != $form->password) {
                $form->password = bcrypt($form->password);
            }
            $form->model()->admin_id = Admin::user() -> id;
        });

        return $form;
    }
}
