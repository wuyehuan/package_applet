<?php
namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\WithMapping;

class StaffExporter extends ExcelExporter implements WithMapping
{
    protected $fileName = '员工列表.xlsx';

    protected $columns = [
        'id' => 'ID',
        'admin_id' => '所属片区',
        'account' => '账号',
        'staff_name' => '员工姓名',
        'car_license' => '车牌号',
        'today_mileage' => '今日里程',
        'total_mileage' => '总里程数',
        'created_at' => '注册时间',
    ];


    /**
     * 设置文件名称
     */
    public function setFileName($name){
        $this -> fileName = $name . '.xlsx';
    }


    /**
     * @param $user
     * @return array
     */
    public function map($user) : array
    {
        return [
            $user -> id,
            data_get($user, 'admin.username'),
            $user -> account,
            $user -> staff_name,
            $user -> car_license,
            $user -> today_mileage,
            $user -> total_mileage,
            $user -> created_at,
        ];
    }
}