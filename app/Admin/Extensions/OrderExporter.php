<?php
namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

use Maatwebsite\Excel\Concerns\FromCollection;    // 导出集合
use Maatwebsite\Excel\Concerns\WithEvents;     // 自动注册事件监听器
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;    // 导出 0 原样显示，不为 null
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;    // 设置工作䈬名称


class OrderExporter extends ExcelExporter implements  WithEvents, WithStrictNullComparison,WithMapping,WithColumnFormatting
{
    protected $fileName = '订单列表.xlsx';

    protected $columns = [
        'id' => 'ID',
        'staff_id' => '揽件员',
        'order_number' => '物流单号',
        'sender_province' => '发货地址(省)',
        'sender_city' => '发货地址(市)',
        'sender_district' => '发货地址(区)',
        'sender_address' => '发货地址(详细地址)',
        'sender_name' => '寄件人',
        'sender_phone' => '寄件人电话',
        'consignee_province' => '收货地址',
        'consignee_city' => '收货地址',
        'consignee_district' => '收货地址',
        'consignee_address' => '收货地址',
        'consignee_name' => '收件人',
        'consignee_phone' => '收件人电话',
        'total_goods_number' => '数量',
        '货品(名称：数量：单位)',
        'is_collection' => '是否代收货款',
        'goods_money' => '代收金额',
        'sign.sign_goods_type' => '代收方式',
        'sender_money' => '运费',
        'pay_type' => '运费方式',
        'goods_remarks' => '货物备注',
        'order_remarks' => '物流备注',
        'order_status' => '状态',
        'created_at' => '下单时间',
        'updated_at' => '签收时间',
        'sign.adress' => '签收地址',
    ];


    /**
     * 设置文件名称
     */
    public function setFileName($name){
        $this -> fileName = $name . '.xlsx';
    }


    /**
     * @param $user
     * @return array
     */
    public function map($user) : array
    {
        // 获取所有的货品
        $data = \DB::table('order_info') -> where('order_id','=',$user -> id) -> get(['goods_name','goods_number','goods_unit']) -> toArray();
        $string = '';
        foreach ($data as $v){
            $string .=  '(' . $v -> goods_name . "：" . $v -> goods_number . "：" . $v -> goods_unit . "\r\n" . ')';
        }
        $signObj = $user -> sign() -> first();
        switch ($signObj -> sign_goods_type){
            case 1:
                $type = '扫码';
                break;
            case 2:
                $type = '现金';
                break;
            default:
                $type = '无';
                break;
        }
        // 1到付 2现金 3扫码'
        $payType = $user -> pay_type;
        switch ($payType){
            case 1:
                $payType = '到付';
                break;
            case 2:
                $payType = '现金';
                break;
            default:
                $payType = '扫码';
                break;
        }
        // order_status` char(8) NOT NULL DEFAULT '0' COMMENT '//订单状态 1运输中 2完成签收 3签收异常 4重新派单 5订单作废',
        $status = $user -> getOriginal('order_status');
        switch ($status){
            case 1:
                $status = '运输中';
                break;
            case 2:
                $status = '完成签收';
                break;
            case 3:
                $status = '签收异常';
                break;
            case 4:
                $status = '重新派单';
                break;
            default:
                $status = '返回收件人';
                break;
        }
        return [
            $user -> id,
            data_get($user, 'staff.staff_name'),
            $user -> order_number,
            $user -> sender_province,
            $user -> sender_city,
            $user -> sender_district,
            $user -> sender_address,
            $user -> sender_name,
            $user -> sender_phone,
            $user -> consignee_province,
            $user -> consignee_city,
            $user -> consignee_district,
            $user -> consignee_address,
            $user -> consignee_name,
            $user -> consignee_phone,
            $user -> total_goods_number,
            $string,
            $user -> is_collection == 1 ? '是' : '否',
            $user -> goods_money,
            $type,
            $user -> sender_money,
            $payType,
            $user -> goods_remarks,
            $user -> order_remarks,
            $status,
            $user -> created_at,
            $user -> updated_at,
            $signObj -> sign_adress,
        ];
    }


    /**
     * 设置宽度
     * @return array
     */
    public function registerEvents(): array
    {
        return [

            AfterSheet::class => function(AfterSheet $event) {
                // 定义列宽度
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth('10');
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth('20');
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth('30');
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth('56');
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth('20');
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('K')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('L')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('M')->setWidth('56');
                $event->sheet->getDelegate()->getColumnDimension('N')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('O')->setWidth('20');
                $event->sheet->getDelegate()->getColumnDimension('P')->setWidth('12');
                $event->sheet->getDelegate()->getColumnDimension('Q')->setWidth('30');
                $event->sheet->getDelegate()->getColumnDimension('R')->setWidth('10');
                $event->sheet->getDelegate()->getColumnDimension('S')->setWidth('20');
                $event->sheet->getDelegate()->getColumnDimension('T')->setWidth('12');
                $event->sheet->getDelegate()->getColumnDimension('U')->setWidth('16');
                $event->sheet->getDelegate()->getColumnDimension('V')->setWidth('12');
                $event->sheet->getDelegate()->getColumnDimension('W')->setWidth('14');
                $event->sheet->getDelegate()->getColumnDimension('X')->setWidth('40');
                $event->sheet->getDelegate()->getColumnDimension('Y')->setWidth('20');
                $event->sheet->getDelegate()->getColumnDimension('Z')->setWidth('30');
                $event->sheet->getDelegate()->getColumnDimension('AA')->setWidth('30');
                $event->sheet->getDelegate()->getColumnDimension('AB')->setWidth('60');
            }
        ];
    }


    /**
     * 设置格式
     * @return array
     */
    public function columnFormats(): array
    {

        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_TEXT,
            'O' => NumberFormat::FORMAT_TEXT,
            'S' => NumberFormat::FORMAT_NUMBER_00,
            'U' => NumberFormat::FORMAT_NUMBER_00,
            'W' => NumberFormat::FORMAT_TEXT,
            'X' => NumberFormat::FORMAT_TEXT,
        ];
    }
}