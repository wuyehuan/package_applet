<?php

namespace App\Admin\Actions\Staff;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class Depart extends RowAction
{
    public $name = '查看发车记录';

    public function handle(Model $model)
    {
        // $model ...

        // return $this->response()->success('Success message.')->refresh();
    }

    /**
     * @return string
     */
    public function href()
    {
        return "/admin/mileage-record?staff_id=" . $this->getKey();
    }

}