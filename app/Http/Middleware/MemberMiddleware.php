<?php

namespace App\Http\Middleware;

use Closure;

class MemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // 动态更改默认配置
        config(['auth.providers.users.model' => \App\Api\StaffModel::class]);
        return $next($request);
    }
}
