<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<!-- 引入 ECharts 文件 -->
		<script src="/asset/map/js/echarts.min.js"></script>
		<style>
			#chart-panel{
				height: 450px;
			}
			.ajax-page a{
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div id="chart-panel"></div>

		{{--订单列表--}}
		<div class="row">
			<div class="col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">订单列表 <span style="color:red;" id="all-city">全国</span></h4>
						{{--<div class="col-lg-9" style="float: left;padding: 0;">--}}
						{{--<button type="button" class="btn btn-sm btn-gradient-success btn-icon-text" onclick="add()">--}}
						{{--<i class="mdi mdi-plus btn-icon-prepend"></i>--}}
						{{--添加订单--}}
						{{--</button>--}}
						{{--<a href="#" class="btn btn-sm btn-gradient-success btn-icon-text">--}}
						{{--<i class="mdi mdi-plus btn-icon-prepend"></i>--}}
						{{--添加订单--}}
						{{--</a>--}}
						{{--</div>--}}
						<div class="col-lg-12" style="padding: 0;">
							<form action="" class="form-inline">
								<div class="form-group col-lg-7" style="padding: 0;">

										<input name="nian" type="text" class="form-control" placeholder="请选择年份" id="nian">
                                        <select id="yue" class="form-control">
                                            <option value="">请选择月份</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
									<button onclick="getOrderDate()" type="button" class="btn button-blue btn-gradient-primary" style="background: #337ab7;color: #fff;">搜索</button>
								</div>
							</form>
						</div>
						<table class="table table-bordered">
							<thead>
							<tr>
								{{--<th width="10%">揽件员</th>--}}
								{{--<th width="10%">订单号</th>--}}
								{{--<th width="15%">发货地址</th>--}}
								{{--<th width="10%">寄件人</th>--}}
								{{--<th width="10%">寄件人电话</th>--}}
								{{--<th width="10%">收件人地址</th>--}}
								{{--<th width="10%">收件人</th>--}}
								{{--<th width="10%">收件人电话</th>--}}
								{{--<th width="5%">数量</th>--}}
								{{--<th width="5">运费</th>--}}
								{{--<th width="5%">是否代收</th>--}}
								{{--<th width="5%">运输状态</th>--}}
								{{--<th width="15%">寄件时间</th>--}}

								<th width="10%">项目地</th>
								<th width="15%">主体</th>
								<th width="10%">车辆</th>
								<th width="10%">人员</th>
								<th width="10%">订单(单)</th>
								<th width="10%">运费(元)</th>
								<th width="10%">代收(元)</th>
								<th width="10%">总里程(KM)</th>
							</tr>
							</thead>
							<tbody id="page-body">

								{{--<tr>--}}
									{{--<td colspan="20">暂无相关数据</td>--}}
								{{--</tr>--}}

								{{--<tr>--}}

									{{--<td>--}}
										{{--<a href="/admin/order/" class="btn btn-sm btn-gradient-dark btn-icon-text">--}}
											{{--订单详情--}}
											{{--<i class="mdi mdi-file-check btn-icon-append"></i>--}}
										{{--</a>--}}
										{{--<button type="button" class="btn btn-sm btn-gradient-danger btn-icon-text" onclick="del(1,this)">--}}
											{{--<i class="mdi mdi-delete btn-icon-prepend"></i>--}}
											{{--删除--}}
										{{--</button>--}}
									{{--</td>--}}
								{{--</tr>--}}

							</tbody>
						</table>
						<div class="box-footer clearfix">
							{{--总共 <b>123</b> 条,分为<b>2</b>页--}}

							<div id="map-page" class="ajax-page"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--start--}}

		<script src="/asset/laydate/laydate.js"></script>
		<script>
			maxOrderNumber = 0
            nian = '';
            yue = '';
            search_scope = 1;
            province_name = '';
            city_name = '';
            function getOrderDate(){
                nian = $("input[name='nian']").val();
                yue = $('#yue').val();
                // console.log(nian,yue,search_scope);

                if(search_scope == 1){
                    getOrder(1,search_scope,'','');
                }else if(search_scope == 2){
                    getOrder(1,search_scope,province_name,city_name);
                }else if(search_scope == 3){
                    getOrder(1,search_scope,province_name,city_name);
                }else{
                    getOrder(1,search_scope,'','');
                }

            }

            laydate.render({
                elem: '#nian'
                ,type: 'year'
            });
            var data = [];
            $(function () {
                $.get('/admin/get-province',function(res){
                    console.log(res.data);
					data = res.data
					maxOrderNumber = res.max
					a()
                })
            })
		</script>

		<script type="text/javascript">

				// var data = [
				// 	{name:"北京",value:200,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"天津",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"河北",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"山西",value:30,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"内蒙古",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"辽宁",value:40,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"吉林",value:50,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"黑龙江",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"上海",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"江苏",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"浙江",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"安徽",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"福建",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"江西",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"山东",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"河南",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"湖北",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"湖南",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"重庆",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"四川",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"贵州",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"云南",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"西藏",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"陕西",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"甘肃",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"青海",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"宁夏",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"新疆",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"广东",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"广西",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"海南",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// 	{name:"台湾",value:0,car_num:0,sender_money:0.00,goods_money:0.00,staff_num:0},
				// ];
			function a() {
                var zhongguo = "/asset/map/address/data-china.json";
                var hainan = "/asset/map/address/data-hainan.json";
                var xizang = "/asset/map/address/data-xizang.json";
                var zhejiang = "/asset/map/address/data-zhejiang.json";
                var yunnan = "/asset/map/address/data-yunnan.json";
                var xinjiang = "/asset/map/address/data-xinjiang.json";
                var tianjin = "/asset/map/address/data-tianjin.json";
                var sichuan = "/asset/map/address/data-sichuan.json";
                var shanxi = "/asset/map/address/data-shanxi.json";
                var shangxi = "/asset/map/address/data-shangxi.json";
                var shanghai = "/asset/map/address/data-shanghai.json";
                var shangdong = "/asset/map/address/data-shangdong.json";
                var qinghai = "/asset/map/address/data-qinghai.json";
                var ningxia = "/asset/map/address/data-ningxia.json";
                var neimenggu = "/asset/map/address/data-neimenggu.json";
                var liaoning = "/asset/map/address/data-liaoning.json";
                var jilin = "/asset/map/address/data-jilin.json";
                var jiangxi = "/asset/map/address/data-jiangxi.json";
                var jiangsu = "/asset/map/address/data-jiangsu.json";
                var hunan = "/asset/map/address/data-hunan.json";
                var hubei = "/asset/map/address/data-hubei.json";
                var henan = "/asset/map/address/data-henan.json";
                var heilongjiang = "/asset/map/address/data-heilongjiang.json";
                var hebei = "/asset/map/address/data-hebei.json";
                var guizhou = "/asset/map/address/data-guizhou.json";
                var guangxi = "/asset/map/address/data-guangxi.json";
                var guangdong = "/asset/map/address/data-guangdong.json";
                var gansu = "/asset/map/address/data-gansu.json";
                var chongqing = "/asset/map/address/data-chongqing.json";
                var aomen = "/asset/map/address/data-aomen.json";
                var anhui = "/asset/map/address/data-anhui.json";
                var beijing = "/asset/map/address/data-beijing.json";
                var fujian = "/asset/map/address/data-fujian.json";
                var xianggang = "/asset/map/address/data-xianggang.json";

                echarts.extendsMap = function (id, opt) {
                    // 实例
                    var chart = this.init(document.getElementById(id));

                    var curGeoJson = {};
                    var cityMap = {
                        '中国': zhongguo,
                        '上海': shanghai,
                        '河北': hebei,
                        '山西': shangxi,
                        '内蒙古': neimenggu,
                        '辽宁': liaoning,
                        '吉林': jilin,
                        '黑龙江': heilongjiang,
                        '江苏': jiangsu,
                        '浙江': zhejiang,
                        '安徽': anhui,
                        '福建': fujian,
                        '江西': jiangxi,
                        '山东': shangdong,
                        '河南': henan,
                        '湖北': hubei,
                        '湖南': hunan,
                        '广东': guangdong,
                        '广西': guangxi,
                        '海南': hainan,
                        '四川': sichuan,
                        '贵州': guizhou,
                        '云南': yunnan,
                        '西藏': xizang,
                        '陕西': shanxi,
                        '甘肃': gansu,
                        '青海': qinghai,
                        '宁夏': ningxia,
                        '新疆': xinjiang,
                        '北京': beijing,
                        '天津': tianjin,
                        '重庆': chongqing,
                        '香港': xianggang,
                        '澳门': aomen
                    };

                    var levelColorMap = {
                        '1': 'rgba(241, 109, 115, .8)',
                        '2': 'rgba(255, 235, 59, .7)',
                        '3': 'rgba(147, 235, 248, 1)'
                    };

                    var defaultOpt = {
                        mapName: 'china', // 地图展示
                        goDown: false, // 是否下钻
                        bgColor: '#fff', // 画布背景色
                        activeArea: [], // 区域高亮,同echarts配置项
                        data: [],
                        // 下钻回调(点击的地图名、实例对象option、实例对象)
                        callback: function (name, option, instance) {
                        }
                    };

                    if (opt) opt = this.util.extend(defaultOpt, opt);

                    // 层级索引
                    var name = [opt.mapName];
                    var idx = 0;
                    var pos = {
                        leftPlus: 115,
                        leftCur: 150,
                        left: 198,
                        top: 50
                    };

                    var line = [
                        [0, 0],
                        [8, 11],
                        [0, 22]
                    ];
                    // style
                    var style = {
                        font: '18px "Microsoft YaHei", sans-serif',
                        textColor: '#eee',
                        lineColor: 'rgba(147, 235, 248, .8)'
                    };

                    var handleEvents = {
                        /**
                         * i 实例对象
                         * o option
                         * n 地图名
                         **/
                        resetOption: function (i, o, n) {
                            var breadcrumb = this.createBreadcrumb(n);
                            var j = name.indexOf(n);
                            var l = o.graphic.length;
                            var _that = this;

							// 前端改 这个接口(/admin/get-province)返回的是全省的数据 我要根据n去请求全国或省的数据(重新写一个借口三个省里面有数据)
							// 三个省里面有什么数据就返回什么数据
							// 然后就没有了 如果没有数据就返回一个空数组给家辉 可以了 如果不行就继续找家辉
							// 就是都合在一个接口 如果是中国就是全省 如果是省就是省里面的所有市

                            $.get('/admin/get-province?key=' + n,function(res) {

                                console.log(res.data, 'dddddddddddddd');

                                data = res.data
                                if (n == '中国') {
									o['tooltip'] = {
										show: true,
										trigger: 'item',
										alwaysShowContent: false,
										backgroundColor: 'rgba(50,50,50,0.7)',
										hideDelay: 100,
										triggerOn: 'mousemove',
										enterable: false,
										//position:['60%','70%'],
										formatter: function (params, ticket, callback) {
											return `
												区域：${params.data.name}<br />
												订单量：${params.data.value}<br />
												员工数：${params.data.all_staff_num}<br />
												车辆：${params.data.all_car_number}<br />
												运费：${params.data.all_sender_money} <br />
												代收：${params.data.all_goods_money} <br />
											`;
										}
									};

									o.series[0].data = data;

									o['visualMap'] = {
										show: true,
										min: 0,
										max: maxOrderNumber,
										left: '10%',
										top: 'bottom',
										calculable: true,
										seriesIndex: [0],
										textStyle: {
											color: '#fff'
										},
										inRange: {
											// color: ['#467bc0', '#04387b'] // 蓝绿
											color: ['#011234', '#1ca2f1'] // 蓝绿
										}
									}
									// 调用
									$('#all-city').html('全国');
									search_scope = 1;
									getOrder(1, '', '');

                                } else {

									province_name = n;
									$('#all-city').html(n);
									search_scope = 2;
									getOrder(1, search_scope, n, '');


									if (data.length) {
										o['tooltip'] = {
											show: true,
											trigger: 'item',
											alwaysShowContent: false,
											backgroundColor: 'rgba(50,50,50,0.7)',
											hideDelay: 100,
											triggerOn: 'mousemove',
											enterable: false,
											//position:['60%','70%'],
											formatter: function (params, ticket, callback) {
												return `
													区域：${params.data.name}<br />
													订单量：${params.data.value}<br />
													员工数：${params.data.all_staff_num}<br />
													车辆：${params.data.all_car_number}<br />
													运费：${params.data.all_sender_money} <br />
													代收：${params.data.all_goods_money} <br />
												`;
											}
										};

										o.series[0].data = data

                                        o['visualMap'] = {
                                            show: true,
                                            min: 0,
                                            max: maxOrderNumber,
                                            left: '10%',
                                            top: 'bottom',
                                            calculable: true,
                                            seriesIndex: [0],
                                            textStyle: {
                                                color: '#fff'
                                            },
                                            inRange: {
                                                // color: ['#467bc0', '#04387b'] // 蓝绿
                                                color: ['#011234', '#1ca2f1'] // 蓝绿
                                            }
                                        }

									} else {
										o['tooltip'] = {
											show: false
										}

                                        o['visualMap'] = {
                                            show: false
                                        }
									}



                                }


                                if (j < 0) {
                                    o.graphic.push(breadcrumb);
                                    o.graphic[0].children[0].shape.x2 = 145;
                                    o.graphic[0].children[1].shape.x2 = 145;
                                    if (o.graphic.length > 2) {
                                        var cityData = [];
                                        var cityJson;
                                        for (var x = 0; x < opt.data.length; x++) {
                                            if (n === opt.data[x].city) {
                                                $([opt.data[x]]).each(function (index, data) {
                                                    cityJson = {
                                                        city: data.city,
                                                        name: data.name,
                                                        value: data.value,
                                                        crew: data.crew,
                                                        company: data.company,
                                                        position: data.position,
                                                        region: data.region
                                                    };
                                                    cityData.push(cityJson)
                                                })
                                            }
                                        }

                                        // if(cityData!=null){
                                        //     o.series[0].data = handleEvents.initSeriesData(cityData);
                                        // }else{
                                        //     o.series[0].data = [];
                                        // }
                                    }
                                    name.push(n);
                                    idx++;
                                } else {
                                    o.graphic.splice(j + 2, l);
                                    if (o.graphic.length <= 2) {
                                        o.graphic[0].children[0].shape.x2 = 60;
                                        o.graphic[0].children[1].shape.x2 = 60;
                                        //o.series[0].data = handleEvents.initSeriesData(opt.data);
                                    }
                                    ;
                                    name.splice(j + 1, l);
                                    idx = j;
                                    pos.leftCur -= pos.leftPlus * (l - j - 1);
                                }
                                ;

                                o.series[0].map = n;
                                // o.geo.zoom = 0.4;
                                i.clear();
                                i.setOption(o);
                                _that.zoomAnimation();
                                opt.callback(n, o, i);
                                
                            })
                        },

                        /**
                         * name 地图名
                         **/
                        createBreadcrumb: function (name) {
                            var cityToPinyin = {
                                '中国': 'zhongguo',
                                '上海': 'shanghai',
                                '河北': 'hebei',
                                '山西': 'shangxi',
                                '内蒙古': 'neimenggu',
                                '辽宁': 'liaoning',
                                '吉林': 'jilin',
                                '黑龙江': 'heilongjiang',
                                '江苏': 'jiangsu',
                                '浙江': 'zhejiang',
                                '安徽': 'anhui',
                                '福建': 'fujian',
                                '江西': 'jiangxi',
                                '山东': 'shangdong',
                                '河南': 'henan',
                                '湖北': 'hubei',
                                '湖南': 'hunan',
                                '广东': 'guangdong',
                                '广西': 'guangxi',
                                '海南': 'hainan',
                                '四川': 'sichuan',
                                '贵州': 'guizhou',
                                '云南': 'yunnan',
                                '西藏': 'xizang',
                                '陕西': 'shanxi',
                                '甘肃': 'gansu',
                                '青海': 'qinghai',
                                '宁夏': 'ningxia',
                                '新疆': 'xinjiang',
                                '北京': 'beijing',
                                '天津': 'tianjin',
                                '重庆': 'chongqing',
                                '香港': 'xianggang',
                                '澳门': 'aomen'
                            };
                            var breadcrumb = {
                                type: 'group',
                                id: name,
                                left: pos.leftCur + pos.leftPlus,
                                top: pos.top + 3,
                                children: [{
                                    type: 'polyline',
                                    left: -90,
                                    top: -5,
                                    shape: {
                                        points: line
                                    },
                                    style: {
                                        stroke: '#fff',
                                        key: name
                                        // lineWidth: 2,
                                    },
                                    onclick: function () {
                                        var name = this.style.key;
                                        handleEvents.resetOption(chart, option, name);
                                    }
                                }, {
                                    type: 'text',
                                    left: -68,
                                    top: 'middle',
                                    style: {
                                        text: name,
                                        textAlign: 'center',
                                        fill: style.textColor,
                                        font: style.font
                                    },
                                    onclick: function () {
                                        var name = this.style.text;
                                        handleEvents.resetOption(chart, option, name);
                                    }
                                }, {
                                    type: 'text',
                                    left: -68,
                                    top: 10,
                                    style: {

                                        name: name,
                                        text: cityToPinyin[name] ? cityToPinyin[name].toUpperCase() : '',
                                        textAlign: 'center',
                                        fill: style.textColor,
                                        font: '12px "Microsoft YaHei", sans-serif',
                                    },
                                    onclick: function () {
                                        var name = this.style.name;
                                        handleEvents.resetOption(chart, option, name);
                                    }
                                }]
                            }

                            pos.leftCur += pos.leftPlus;

                            return breadcrumb;
                        },

                        // 设置effectscatter
                        initSeriesData: function (data) {
                            var temp = [];
                            for (var i = 0; i < data.length; i++) {
                                //var geoCoord = geoCoordMap[data[i].name];
                                // if (geoCoord) {
                                temp.push({
                                    name: data[i].name,
                                    value: data[i].value,
                                    crew: data[i].crew,
                                    company: data[i].company,
                                    position: data[i].position,
                                    region: data[i].region,
                                    // tooltip : {
                                    //     formatter : 'dfdf'
                                    // }
                                });
                                // }
                            }
                            ;

                            //console.log(temp)
                            return temp;
                        },
                        zoomAnimation: function () {
                            var count = null;
                            var zoom = function (per) {
                                if (!count) count = per;
                                count = count + per;
                                // console.log(per,count);
                                chart.setOption({
                                    geo: {
                                        zoom: count
                                    }
                                });
                                if (count < 1) window.requestAnimationFrame(function () {
                                    zoom(0.2);
                                });
                            };
                            window.requestAnimationFrame(function () {
                                zoom(0.2);
                            });
                        }
                    };

                    var option = {
                        backgroundColor: opt.bgColor,
                        // 悬浮提示
                        tooltip: {
                            show: true,
                            trigger: 'item',
                            alwaysShowContent: false,
                            backgroundColor: 'rgba(50,50,50,0.7)',
                            hideDelay: 100,
                            triggerOn: 'mousemove',
                            enterable: false,
                            //position:['60%','70%'],
                            formatter: function (params, ticket, callback) {
                                return `
										区域：${params.data.name}<br />
										订单量：${params.data.value}<br />
										员工数：${params.data.all_staff_num}<br />
										车辆：${params.data.all_car_number}<br />
										运费：${params.data.all_sender_money} <br />
										代收：${params.data.all_goods_money} <br />
									`;
                            }
                        },
                        visualMap: {
                            show: true,
                            min: 0,
                            max: maxOrderNumber,
                            left: '10%',
                            top: 'bottom',
                            calculable: true,
                            seriesIndex: [0],
                            textStyle:{
                                color:'#fff'
							},
                            inRange: {
                                color: ['#011234', '#1ca2f1'] // 蓝绿
                            }
                        },
                        //  图标横线
                        graphic: [{
                            type: 'group',
                            left: pos.left,
                            top: pos.top - 4,
                            children: [{
                                type: 'line',
                                left: 0,
                                top: -20,
                                shape: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 60,
                                    y2: 0
                                },
                                style: {
                                    stroke: style.lineColor,
                                }
                            }, {
                                type: 'line',
                                left: 0,
                                top: 20,
                                shape: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 60,
                                    y2: 0
                                },
                                style: {
                                    stroke: style.lineColor,
                                }
                            }]
                        },
                            {
                                id: name[idx],
                                type: 'group',
                                left: pos.left + 2,
                                top: pos.top,
                                children: [{
                                    type: 'polyline',
                                    left: 90,
                                    top: -12,
                                    shape: {
                                        points: line
                                    },
                                    style: {
                                        stroke: 'transparent',
                                        key: name[0]
                                    },
                                    onclick: function () {
                                        var name = this.style.key;
                                        handleEvents.resetOption(chart, option, name);
                                    }
                                }, {
                                    type: 'text',
                                    left: 0,
                                    top: 'middle',
                                    style: {
                                        text: '中国',
                                        textAlign: 'center',
                                        fill: style.textColor,
                                        font: style.font
                                    },
                                    onclick: function () {
                                        handleEvents.resetOption(chart, option, '中国');
                                    }
                                }, {
                                    type: 'text',
                                    left: 0,
                                    top: 10,
                                    style: {
                                        text: 'China',
                                        textAlign: 'center',
                                        fill: style.textColor,
                                        font: '12px "Microsoft YaHei", sans-serif',
                                    },
                                    onclick: function () {
                                        handleEvents.resetOption(chart, option, '中国');
                                    }
                                }]
                            }],

                        series: [

                            {
                                name: '',
                                type: 'map',
                                map: '中国',
                                roam: true,
                                aspectScale: 0.75, //长宽比
                                showLegendSymbol: false, // 存在legend时显示
                                label: {
                                    normal: {
                                        show: true,
                                        textStyle: {
                                            color: '#fff'
                                        }
                                    },
                                    emphasis: {
                                        show: false,
                                        textStyle: {
                                            color: '#fff'
                                        }
                                    }
                                },
                                roam: true,
                                itemStyle: {
                                    normal: {
                                        areaColor: '#011234',
                                        borderColor: '#1180c7',
                                    },
                                    emphasis: {
                                        areaColor: '#4499d0',
                                    }
                                },
                                animation: false,
                                // 文本位置修正

                                data: data

                            }
                        ]
                    };

                    // console.log(option)
                    chart.setOption(option);
                    // 添加事件
                    chart.on('click', function (params) {

                        if(isNaN(params.value) && params.name != ''){
                            search_scope = 3;
                            $('#all-city').html(params.name);
                            city_name = params.name;
                            getOrder(1,search_scope,'',params.name);
							console.log(params.name);
						}

                        var _self = this;
                        //console.log(opt.goDown, params.name, name[idx])
                        if (opt.goDown && params.name !== name[idx]) {
                            if (cityMap[params.name]) {
                                var url = cityMap[params.name];

                                $.get(url, function (response) {
                                    
                                    // 记号
                                    // let o = {};
                                    // o.backgroundColor = option.backgroundColor
                                    // o.graphic = option.graphic
                                    // o.series = option.series
                                    // o.tooltip = {
                                    //     show : true
                                    // }
                                    // o.visualMap = option.visualMap


                                    curGeoJson = response;
                                    echarts.registerMap(params.name, response);
                                    handleEvents.resetOption(_self, option, params.name);
                                });
                            }
                        }
                    });

                    chart.on('mouseover', function (params) {

                    });

                    chart.on('mouseout', function (params) {
                        // console.log(params, 111)
                    });

                    chart.setMap = function (mapName) {
                        var _self = this;
                        if (mapName.indexOf('市') < 0) mapName = mapName + '市';
                        var citySource = cityMap[mapName];
                        if (citySource) {

                            var url = './map/' + citySource + '.json';
                            $.get(url, function (response) {
                                curGeoJson = response;
                                echarts.registerMap(mapName, response);
                                handleEvents.resetOption(_self, option, mapName);
                            });
                        }

                    };

                    return chart;
                };

                $.getJSON(zhongguo, function (geoJson) {
                    echarts.registerMap('中国', geoJson);
                    var myChart = echarts.extendsMap('chart-panel', {
                        bgColor: '#152643', // 画布背景色
                        mapName: '中国', // 地图名
                        goDown: true, // 是否下钻
                        // 下钻回调
                        data: [],
                        callback: function (name, option, instance) {
                            //console.log(name, option, instance);
                        },

                    });
                })
            }

			function getOrder(page = 1,scope = '',province = '',city = ''){
				$.get('/admin/board/order?page=' + page + '&scope=' + scope + '&province=' + province + '&city=' + city + '&year=' + nian + '&month=' + yue,function(res){

					if(res.error_code == 0){
						var html = '';
						var data = res.data.data;
						for (var i = 0; i < data.length; i++){

							html +=
								`
			<tr>
			<td>${data[i].province}${data[i].city}</td>
			<td>${data[i].company_name}</td>
			<td>${data[i].all_car_number}</td>
			<td>${data[i].all_staff_name}</td>
			<td>${data[i].order_num}</td>
			<td>${data[i].all_sender_money}</td>
			<td>${data[i].all_goods_money}</td>
			<td>0</td>
			</tr>
			`;
						}

						if(html == ''){
							html = "<tr><td colspan='20'>暂无相关数据</td></tr>"
						}
						$('#page-body').html(html);
					}

					$('#map-page').html(res.page);

					$('.ajax-page .pagination').find('a').each(function(e){
						let page=$(this).attr('href').split('page=')[1];
						$(this).removeAttr('href'); // 干掉href属性
						var _t = `
			getOrder(${page},${res.param['scope']},'${res.param['province']}','${res.param['city']}')
			`;
						$(this).attr("onclick", _t); // 新增onclick事件
					});
				});
			}

			getOrder();
            // });

            /*分享需要的*/
		</script>

		{{--end--}}

		{{--<script type="text/javascript">--}}
			{{--// 注：源码就是下面这些，只要复制到开发工具就行，然后把地图json数据（下载地址：https://share.weiyun.com/5x12K4r）的路径放对，并且要导入echarts就可以渲染出来地图了--}}
			{{--var zhongguo = "/asset/map/address/data-china.json";--}}
			{{--var hainan = "/asset/map/address/data-hainan.json";--}}
			{{--var xizang = "/asset/map/address/data-xizang.json";--}}
			{{--var zhejiang = "/asset/map/address/data-zhejiang.json";--}}
			{{--var yunnan = "/asset/map/address/data-yunnan.json";--}}
			{{--var xinjiang = "/asset/map/address/data-xinjiang.json";--}}
			{{--var tianjin = "/asset/map/address/data-tianjin.json";--}}
			{{--var sichuan = "/asset/map/address/data-sichuan.json";--}}
			{{--var shanxi = "/asset/map/address/data-shanxi.json";--}}
			{{--var shangxi = "/asset/map/address/data-shangxi.json";--}}
			{{--var shanghai = "/asset/map/address/data-shanghai.json";--}}
			{{--var shangdong = "/asset/map/address/data-shangdong.json";--}}
			{{--var qinghai = "/asset/map/address/data-qinghai.json";--}}
			{{--var ningxia = "/asset/map/address/data-ningxia.json";--}}
			{{--var neimenggu = "/asset/map/address/data-neimenggu.json";--}}
			{{--var liaoning = "/asset/map/address/data-liaoning.json";--}}
			{{--var jilin = "/asset/map/address/data-jilin.json";--}}
			{{--var jiangxi = "/asset/map/address/data-jiangxi.json";--}}
			{{--var jiangsu = "/asset/map/address/data-jiangsu.json";--}}
			{{--var hunan = "/asset/map/address/data-hunan.json";--}}
			{{--var hubei = "/asset/map/address/data-hubei.json";--}}
			{{--var henan = "/asset/map/address/data-henan.json";--}}
			{{--var heilongjiang = "/asset/map/address/data-heilongjiang.json";--}}
			{{--var hebei = "/asset/map/address/data-hebei.json";--}}
			{{--var guizhou = "/asset/map/address/data-guizhou.json";--}}
			{{--var guangxi = "/asset/map/address/data-guangxi.json";--}}
			{{--var guangdong = "/asset/map/address/data-guangdong.json";--}}
			{{--var gansu = "/asset/map/address/data-gansu.json";--}}
			{{--var chongqing = "/asset/map/address/data-chongqing.json";--}}
			{{--var aomen = "/asset/map/address/data-aomen.json";--}}
			{{--var anhui = "/asset/map/address/data-anhui.json";--}}
			{{--var beijing = "/asset/map/address/data-beijing.json";--}}
			{{--var fujian = "/asset/map/address/data-fujian.json";--}}
			{{--var xianggang = "/asset/map/address/data-xianggang.json";--}}

			{{--echarts.extendsMap = function(id, opt) {--}}
				{{--// 实例--}}
				{{--var chart = this.init(document.getElementById(id));--}}

				{{--var curGeoJson = {};--}}
				{{--var cityMap = {--}}
					{{--'中国': zhongguo,--}}
					{{--'上海': shanghai,--}}
					{{--'河北': hebei,--}}
					{{--'山西': shangxi,--}}
					{{--'内蒙古': neimenggu,--}}
					{{--'辽宁': liaoning,--}}
					{{--'吉林': jilin,--}}
					{{--'黑龙江': heilongjiang,--}}
					{{--'江苏': jiangsu,--}}
					{{--'浙江': zhejiang,--}}
					{{--'安徽': anhui,--}}
					{{--'福建': fujian,--}}
					{{--'江西': jiangxi,--}}
					{{--'山东': shangdong,--}}
					{{--'河南': henan,--}}
					{{--'湖北': hubei,--}}
					{{--'湖南': hunan,--}}
					{{--'广东': guangdong,--}}
					{{--'广西': guangxi,--}}
					{{--'海南': hainan,--}}
					{{--'四川': sichuan,--}}
					{{--'贵州': guizhou,--}}
					{{--'云南': yunnan,--}}
					{{--'西藏': xizang,--}}
					{{--'陕西': shanxi,--}}
					{{--'甘肃': gansu,--}}
					{{--'青海': qinghai,--}}
					{{--'宁夏': ningxia,--}}
					{{--'新疆': xinjiang,--}}
					{{--'北京': beijing,--}}
					{{--'天津': tianjin,--}}
					{{--'重庆': chongqing,--}}
					{{--'香港': xianggang,--}}
					{{--'澳门': aomen--}}
				{{--};--}}

				{{--var levelColorMap = {--}}
					{{--'1': 'rgba(241, 109, 115, .8)',--}}
					{{--'2': 'rgba(255, 235, 59, .7)',--}}
					{{--'3': 'rgba(147, 235, 248, 1)'--}}
				{{--};--}}

				{{--var defaultOpt = {--}}
					{{--mapName: 'china', // 地图展示--}}
					{{--goDown: false, // 是否下钻--}}
					{{--bgColor: '#404a59', // 画布背景色--}}
					{{--activeArea: [], // 区域高亮,同echarts配置项--}}
					{{--data: [],--}}
					{{--// 下钻回调(点击的地图名、实例对象option、实例对象)--}}
					{{--callback: function(name, option, instance) {}--}}
				{{--};--}}
				{{--if (opt) opt = this.util.extend(defaultOpt, opt);--}}

				{{--// 层级索引--}}
				{{--var name = [opt.mapName];--}}
				{{--var idx = 0;--}}
				{{--var pos = {--}}
					{{--leftPlus: 115,--}}
					{{--leftCur: 150,--}}
					{{--left: 198,--}}
					{{--top: 50--}}
				{{--};--}}

				{{--var line = [--}}
					{{--[0, 0],--}}
					{{--[8, 11],--}}
					{{--[0, 22]--}}
				{{--];--}}
				{{--// style--}}
				{{--var style = {--}}
					{{--font: '18px "Microsoft YaHei", sans-serif',--}}
					{{--textColor: '#eee',--}}
					{{--lineColor: 'rgba(147, 235, 248, .8)'--}}
				{{--};--}}

				{{--var handleEvents = {--}}
					{{--/**--}}
					 {{--* i 实例对象--}}
					 {{--* o option--}}
					 {{--* n 地图名--}}
					 {{--**/--}}
					{{--resetOption: function(i, o, n) {--}}
						{{--var breadcrumb = this.createBreadcrumb(n);--}}
						{{--var j = name.indexOf(n);--}}
						{{--var l = o.graphic.length;--}}
						{{--if (j < 0) {--}}
							{{--o.graphic.push(breadcrumb);--}}
							{{--o.graphic[0].children[0].shape.x2 = 145;--}}
							{{--o.graphic[0].children[1].shape.x2 = 145;--}}
							{{--if (o.graphic.length > 2) {--}}
								{{--var cityData = [];--}}
								{{--var cityJson;--}}
								{{--for (var x = 0; x < opt.data.length; x++) {--}}
									{{--if(n === opt.data[x].city){--}}
										{{--$([opt.data[x]]).each(function(index,data){--}}
											{{--cityJson = {city:data.city,name:data.name,value:data.value,crew:data.crew,company:data.company,position:data.position,region:data.region};--}}
											{{--cityData.push(cityJson)--}}
										{{--})--}}
									{{--}--}}
								{{--}--}}

								{{--if(cityData!=null){--}}
									{{--o.series[0].data = handleEvents.initSeriesData(cityData);--}}
								{{--}else{--}}
									{{--o.series[0].data = [];--}}
								{{--}--}}


							{{--}--}}
							{{--name.push(n);--}}
							{{--idx++;--}}
						{{--} else {--}}
							{{--o.graphic.splice(j + 2, l);--}}
							{{--if (o.graphic.length <= 2) {--}}
								{{--o.graphic[0].children[0].shape.x2 = 60;--}}
								{{--o.graphic[0].children[1].shape.x2 = 60;--}}
								{{--o.series[0].data = handleEvents.initSeriesData(opt.data);--}}
							{{--};--}}
							{{--name.splice(j + 1, l);--}}
							{{--idx = j;--}}
							{{--pos.leftCur -= pos.leftPlus * (l - j - 1);--}}
						{{--};--}}

						{{--o.geo.map = n;--}}
						{{--o.geo.zoom = 0.4;--}}
						{{--i.clear();--}}
						{{--i.setOption(o);--}}
						{{--this.zoomAnimation();--}}
						{{--opt.callback(n, o, i);--}}
					{{--},--}}

					{{--/**--}}
					 {{--* name 地图名--}}
					 {{--**/--}}
					{{--createBreadcrumb: function(name) {--}}
						{{--var cityToPinyin = {--}}
							{{--'中国': 'zhongguo',--}}
							{{--'上海': 'shanghai',--}}
							{{--'河北': 'hebei',--}}
							{{--'山西': 'shangxi',--}}
							{{--'内蒙古': 'neimenggu',--}}
							{{--'辽宁': 'liaoning',--}}
							{{--'吉林': 'jilin',--}}
							{{--'黑龙江': 'heilongjiang',--}}
							{{--'江苏': 'jiangsu',--}}
							{{--'浙江': 'zhejiang',--}}
							{{--'安徽': 'anhui',--}}
							{{--'福建': 'fujian',--}}
							{{--'江西': 'jiangxi',--}}
							{{--'山东': 'shangdong',--}}
							{{--'河南': 'henan',--}}
							{{--'湖北': 'hubei',--}}
							{{--'湖南': 'hunan',--}}
							{{--'广东': 'guangdong',--}}
							{{--'广西': 'guangxi',--}}
							{{--'海南': 'hainan',--}}
							{{--'四川': 'sichuan',--}}
							{{--'贵州': 'guizhou',--}}
							{{--'云南': 'yunnan',--}}
							{{--'西藏': 'xizang',--}}
							{{--'陕西': 'shanxi',--}}
							{{--'甘肃': 'gansu',--}}
							{{--'青海': 'qinghai',--}}
							{{--'宁夏': 'ningxia',--}}
							{{--'新疆': 'xinjiang',--}}
							{{--'北京': 'beijing',--}}
							{{--'天津': 'tianjin',--}}
							{{--'重庆': 'chongqing',--}}
							{{--'香港': 'xianggang',--}}
							{{--'澳门': 'aomen'--}}
						{{--};--}}
						{{--var breadcrumb = {--}}
							{{--type: 'group',--}}
							{{--id: name,--}}
							{{--left: pos.leftCur + pos.leftPlus,--}}
							{{--top: pos.top + 3,--}}
							{{--children: [{--}}
								{{--type: 'polyline',--}}
								{{--left: -90,--}}
								{{--top: -5,--}}
								{{--shape: {--}}
									{{--points: line--}}
								{{--},--}}
								{{--style: {--}}
									{{--stroke: '#fff',--}}
									{{--key: name--}}
									{{--// lineWidth: 2,--}}
								{{--},--}}
								{{--onclick: function() {--}}
									{{--var name = this.style.key;--}}
									{{--handleEvents.resetOption(chart, option, name);--}}
								{{--}--}}
							{{--}, {--}}
								{{--type: 'text',--}}
								{{--left: -68,--}}
								{{--top: 'middle',--}}
								{{--style: {--}}
									{{--text: name,--}}
									{{--textAlign: 'center',--}}
									{{--fill: style.textColor,--}}
									{{--font: style.font--}}
								{{--},--}}
								{{--onclick: function() {--}}
									{{--var name = this.style.text;--}}
									{{--handleEvents.resetOption(chart, option, name);--}}
								{{--}--}}
							{{--}, {--}}
								{{--type: 'text',--}}
								{{--left: -68,--}}
								{{--top: 10,--}}
								{{--style: {--}}

									{{--name: name,--}}
									{{--text: cityToPinyin[name] ? cityToPinyin[name].toUpperCase() : '',--}}
									{{--textAlign: 'center',--}}
									{{--fill: style.textColor,--}}
									{{--font: '12px "Microsoft YaHei", sans-serif',--}}
								{{--},--}}
								{{--onclick: function() {--}}
									{{--// console.log(this.style);--}}
									{{--var name = this.style.name;--}}
									{{--handleEvents.resetOption(chart, option, name);--}}
								{{--}--}}
							{{--}]--}}
						{{--}--}}

						{{--pos.leftCur += pos.leftPlus;--}}

						{{--return breadcrumb;--}}
					{{--},--}}

				   {{--// 设置effectscatter--}}
					{{--initSeriesData: function(data) {--}}
						{{--var temp = [];--}}
						{{--for (var i = 0; i < data.length; i++) {--}}
							{{--var geoCoord = geoCoordMap[data[i].name];--}}
							{{--if (geoCoord) {--}}
								{{--temp.push({--}}
									{{--name: data[i].name,--}}
									{{--value: geoCoord.concat(data[i].value),--}}
									{{--crew:data[i].crew,--}}
									{{--company:data[i].company,--}}
									{{--position:data[i].position,--}}
									{{--region:data[i].region--}}
								{{--});--}}
							{{--}--}}
						{{--};--}}
						{{--return temp;--}}
					{{--},--}}
					{{--zoomAnimation: function() {--}}
						{{--var count = null;--}}
						{{--var zoom = function(per) {--}}
							{{--if (!count) count = per;--}}
							{{--count = count + per;--}}
							{{--// console.log(per,count);--}}
							{{--chart.setOption({--}}
								{{--geo: {--}}
									{{--zoom: count--}}
								{{--}--}}
							{{--});--}}
							{{--if (count < 1) window.requestAnimationFrame(function() {--}}
								{{--zoom(0.2);--}}
							{{--});--}}
						{{--};--}}
						{{--window.requestAnimationFrame(function() {--}}
							{{--zoom(0.2);--}}
						{{--});--}}
					{{--}--}}
				{{--};--}}

				{{--var option = {--}}
					{{--backgroundColor: opt.bgColor,--}}
					 {{--tooltip: {--}}
						{{--show: true,--}}
						{{--trigger:'item',--}}
						{{--alwaysShowContent:false,--}}
						{{--backgroundColor:'rgba(50,50,50,0.7)',--}}
						{{--hideDelay:100,--}}
						{{--triggerOn:'mousemove',--}}
						{{--enterable:true,--}}
						{{--position:['60%','70%'],--}}
						{{--formatter:function(params, ticket, callback){--}}
							{{--return '简称：'+params.data.name+'<br/>'+'机组：'+params.data.crew+'万千瓦'+'<br/>'+'公司名称：'+params.data.company+'<br/>'+'所属大区：'+params.data.region+'<br/>'+'所在位置：'+params.data.position--}}
						{{--}--}}
					{{--},--}}
					{{--graphic: [{--}}
						{{--type: 'group',--}}
						{{--left: pos.left,--}}
						{{--top: pos.top - 4,--}}
						{{--children: [{--}}
							{{--type: 'line',--}}
							{{--left: 0,--}}
							{{--top: -20,--}}
							{{--shape: {--}}
								{{--x1: 0,--}}
								{{--y1: 0,--}}
								{{--x2: 60,--}}
								{{--y2: 0--}}
							{{--},--}}
							{{--style: {--}}
								{{--stroke: style.lineColor,--}}
							{{--}--}}
						{{--}, {--}}
							{{--type: 'line',--}}
							{{--left: 0,--}}
							{{--top: 20,--}}
							{{--shape: {--}}
								{{--x1: 0,--}}
								{{--y1: 0,--}}
								{{--x2: 60,--}}
								{{--y2: 0--}}
							{{--},--}}
							{{--style: {--}}
								{{--stroke: style.lineColor,--}}
							{{--}--}}
						{{--}]--}}
					{{--},--}}
					{{--{--}}
						{{--id: name[idx],--}}
						{{--type: 'group',--}}
						{{--left: pos.left + 2,--}}
						{{--top: pos.top,--}}
						{{--children: [{--}}
							{{--type: 'polyline',--}}
							{{--left: 90,--}}
							{{--top: -12,--}}
							{{--shape: {--}}
								{{--points: line--}}
							{{--},--}}
							{{--style: {--}}
								{{--stroke: 'transparent',--}}
								{{--key: name[0]--}}
							{{--},--}}
							{{--onclick: function() {--}}
								{{--var name = this.style.key;--}}
								{{--handleEvents.resetOption(chart, option, name);--}}
							{{--}--}}
						{{--}, {--}}
							{{--type: 'text',--}}
							{{--left: 0,--}}
							{{--top: 'middle',--}}
							{{--style: {--}}
								{{--text: '中国',--}}
								{{--textAlign: 'center',--}}
								{{--fill: style.textColor,--}}
								{{--font: style.font--}}
							{{--},--}}
							{{--onclick: function() {--}}
								{{--handleEvents.resetOption(chart, option, '中国');--}}

								{{--$('#all-city').html('全国');--}}
                                {{--// 查找所有的数据--}}
								{{--search_scope = 1;--}}
                                {{--getOrder(1,'','')--}}
							{{--}--}}
						{{--}, {--}}
							{{--type: 'text',--}}
							{{--left: 0,--}}
							{{--top: 10,--}}
							{{--style: {--}}
								{{--text: 'China',--}}
								{{--textAlign: 'center',--}}
								{{--fill: style.textColor,--}}
								{{--font: '12px "Microsoft YaHei", sans-serif',--}}
							{{--},--}}
							{{--onclick: function() {--}}
								{{--handleEvents.resetOption(chart, option, '中国');--}}
							{{--}--}}
						{{--}]--}}
					{{--}],--}}
					{{--geo: {--}}
						{{--map: opt.mapName,--}}
						{{--roam: true,--}}
						{{--zoom: 1,--}}
						{{--label: {--}}
							{{--normal: {--}}
								{{--show: true,--}}
								{{--textStyle: {--}}
									{{--color: '#fff'--}}
								{{--}--}}
							{{--},--}}
							{{--emphasis: {--}}
								{{--textStyle: {--}}
									{{--color: '#fff'--}}
								{{--}--}}
							{{--}--}}
						{{--},--}}
						{{--itemStyle: {--}}
							{{--normal: {--}}
								{{--borderColor: 'rgba(147, 235, 248, 1)',--}}
								{{--borderWidth: 1,--}}
								{{--areaColor: {--}}
									{{--type: 'radial',--}}
									{{--x: 0.5,--}}
									{{--y: 0.5,--}}
									{{--r: 0.8,--}}
									{{--colorStops: [{--}}
										{{--offset: 0,--}}
										{{--color: 'rgba(147, 235, 248, 0)' // 0% 处的颜色--}}
									{{--}, {--}}
										{{--offset: 1,--}}
										{{--color: 'rgba(147, 235, 248, .2)' // 100% 处的颜色--}}
									{{--}],--}}
									{{--globalCoord: false // 缺省为 false--}}
								{{--},--}}
								{{--shadowColor: 'rgba(128, 217, 248, 1)',--}}
								{{--// shadowColor: 'rgba(255, 255, 255, 1)',--}}
								{{--shadowOffsetX: -2,--}}
								{{--shadowOffsetY: 2,--}}
								{{--shadowBlur: 10--}}
							{{--},--}}
							{{--emphasis: {--}}
								{{--areaColor: '#389BB7',--}}
								{{--borderWidth: 0--}}
							{{--}--}}
						{{--},--}}
						{{--regions: opt.activeArea.map(function(item) {--}}
							{{--if (typeof item !== 'string') {--}}
								{{--return {--}}
									{{--name: item.name,--}}
									{{--itemStyle: {--}}
										{{--normal: {--}}
											{{--areaColor: item.areaColor || '#389BB7'--}}
										{{--}--}}
									{{--},--}}
									{{--label: {--}}
										{{--normal: {--}}
											{{--show: item.showLabel,--}}
											{{--textStyle: {--}}
												{{--color: '#fff'--}}
											{{--}--}}
										{{--}--}}
									{{--}--}}
								{{--}--}}
							{{--} else {--}}
								{{--return {--}}
									{{--name: item,--}}
									{{--itemStyle: {--}}
										{{--normal: {--}}
											{{--borderColor: '#91e6ff',--}}
											{{--areaColor: '#389BB7'--}}
										{{--}--}}
									{{--}--}}
								{{--}--}}
							{{--}--}}
						{{--})--}}
					{{--},--}}
					{{--series: [{--}}
						{{--type: 'effectScatter',--}}
						{{--coordinateSystem: 'geo',--}}
						{{--showEffectOn: 'render',--}}
						{{--rippleEffect: {--}}
							{{--period:15,--}}
							{{--scale: 4,--}}
							{{--brushType: 'fill'--}}
						{{--},--}}
						{{--hoverAnimation: true,--}}
						{{--itemStyle: {--}}
							{{--normal: {--}}
								{{--color: '#FFC848',--}}
								{{--shadowBlur: 10,--}}
								{{--shadowColor: '#333'--}}
							{{--}--}}
						{{--},--}}
						{{--data: handleEvents.initSeriesData(opt.data)--}}
					{{--}]--}}
				{{--};--}}

				{{--chart.setOption(option);--}}
				{{--// 添加事件--}}
				{{--chart.on('click', function(params) {--}}
					{{--var _self = this;--}}

					{{--if (opt.goDown && params.name !== name[idx]) {--}}

					    {{--// 验证是否点击了全国--}}
						{{--if(params.name != ''){--}}
                            {{--$('#all-city').html(params.name);--}}
                            {{--search_scope = 3;--}}
                        {{--}else{--}}
                            {{--search_scope = false;--}}
						{{--}--}}

					    {{--if (cityMap[params.name]) {--}}

                            {{--search_scope = 2;--}}

							{{--var url = cityMap[params.name];--}}
							{{--$.get(url, function(response) {--}}

								{{--curGeoJson = response;--}}
								{{--echarts.registerMap(params.name, response);--}}
								{{--handleEvents.resetOption(_self, option, params.name);--}}
							{{--});--}}
						{{--}--}}

						{{--if(!params.name){--}}
						    {{--if(name[idx] != '中国'){--}}
                                {{--$('#all-city').html(name[idx]);--}}
                                {{--getOrder(1,2,name[idx]);--}}
							{{--}--}}
						{{--}else{--}}
                            {{--if(search_scope){--}}
                                {{--// 省--}}
                                {{--// 请求接口--}}
                                {{--if(search_scope == 2){--}}
									{{--province_name = params.name;--}}
                                    {{--// 获取订单信息--}}
                                    {{--getOrder(1,search_scope,params.name,'');--}}
                                {{--}else{--}}
                                    {{--city_name = params.name;--}}
                                    {{--// 市--}}
                                    {{--getOrder(1,search_scope,'',params.name);--}}
                                {{--}--}}
                            {{--}--}}
                        {{--}--}}
					{{--}--}}
				{{--});--}}

				{{--chart.setMap = function(mapName) {--}}
					{{--var _self = this;--}}
					{{--if (mapName.indexOf('市') < 0) mapName = mapName + '市';--}}
					{{--var citySource = cityMap[mapName];--}}
					{{--if (citySource) {--}}
						{{--var url = './map/' + citySource + '.json';--}}
						{{--$.get(url, function(response) {--}}
							{{--// console.log(response);--}}
							{{--curGeoJson = response;--}}
							{{--echarts.registerMap(mapName, response);--}}
							{{--handleEvents.resetOption(_self, option, mapName);--}}
						{{--});--}}
					{{--}--}}

				{{--};--}}

				{{--return chart;--}}
			{{--};--}}

			{{--$.getJSON(zhongguo, function(geoJson) {--}}
				{{--echarts.registerMap('中国', geoJson);--}}
				{{--var myChart = echarts.extendsMap('chart-panel', {--}}
					{{--// bgColor: '#154e90', // 画布背景色--}}
					{{--bgColor: '#004169', // 画布背景色--}}
					{{--mapName: '中国', // 地图名--}}
					{{--goDown: true, // 是否下钻--}}
					{{--// 下钻回调--}}
					{{--callback: function(name, option, instance) {--}}
						{{--//console.log(name, option, instance);--}}
					{{--},--}}

				{{--});--}}
			{{--})--}}


			{{--// 分割线--}}
			{{--function getOrder(page = 1,scope = '',province = '',city = ''){--}}
			    {{--$.get('/admin/board/order?page=' + page + '&scope=' + scope + '&province=' + province + '&city=' + city + '&year=' + nian + '&month=' + yue,function(res){--}}

			        {{--if(res.error_code == 0){--}}
						{{--var html = '';--}}
						{{--var data = res.data.data;--}}
						{{--for (var i = 0; i < data.length; i++){--}}

                            {{--html +=--}}
                                {{--`--}}
									{{--<tr>--}}
										{{--<td>${data[i].province}${data[i].city}</td>--}}
										{{--<td>${data[i].company_name}</td>--}}
										{{--<td>${data[i].all_car_number}</td>--}}
										{{--<td>${data[i].all_staff_name}</td>--}}
										{{--<td>${data[i].order_num}</td>--}}
										{{--<td>${data[i].all_sender_money}</td>--}}
										{{--<td>${data[i].all_goods_money}</td>--}}
										{{--<td>0</td>--}}
									{{--</tr>--}}
								{{--`;--}}
                        {{--}--}}

                        {{--if(html == ''){--}}
							{{--html = "<tr><td colspan='20'>暂无相关数据</td></tr>"--}}
                        {{--}--}}
                        {{--$('#page-body').html(html);--}}
					{{--}--}}

			        {{--$('#map-page').html(res.page);--}}

                    {{--$('.ajax-page .pagination').find('a').each(function(e){--}}
                        {{--let page=$(this).attr('href').split('page=')[1];--}}
                        {{--$(this).removeAttr('href'); // 干掉href属性--}}
						{{--var _t = `--}}
							{{--getOrder(${page},${res.param['scope']},'${res.param['province']}','${res.param['city']}')--}}
						{{--`;--}}
                        {{--$(this).attr("onclick", _t); // 新增onclick事件--}}
                    {{--});--}}
				{{--});--}}
			{{--}--}}

			{{--getOrder();--}}

		{{--</script>--}}
	</body>
</html>
