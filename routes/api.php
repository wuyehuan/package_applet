<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['middleware' => ['member_change','member_login']],function(){

    //
});

// 路由
Route::group(['prefix' => 'v1'],function(){

    Route::group(['namespace' => 'Api','prefix' => 'staff'],function(){
        //小程序登录
       // Route::post('wxlogin','LoginController@wxlogin');
        //测试登录
        Route::post('login','LoginController@Login');
        //图片处理
        Route::post('mileage_img','StaffController@mileage_img');
        //员工发车
        Route::post('start_car','StaffController@start_car');
        //员工交车
        Route::post('end_car','StaffController@end_car');
        //员工详情
        Route::post('staff_info','StaffController@staff_info');
        //员工订单
        Route::post('my_order','StaffController@my_order');
        //刷新token
        Route::post('refresh_token','LoginController@refresh_token');

        Route::post('editStaff','StaffController@editStaff');

        Route::post('editPassword','StaffController@editPassword');

        Route::post('edit_head','StaffController@edit_head');

    });

    Route::group(['namespace' => 'Api','prefix' => 'order'],function(){
        //
        Route::post('addOrder','OrderController@addOrder');

        Route::post('order_list','OrderController@order_list');

        Route::post('up_img','OrderController@up_img');

        Route::post('sign','OrderController@Sign');

        Route::post('sign_abnormal','OrderController@sign_abnormal');

        Route::post('order_info','OrderController@order_info');

        Route::post('again_deliver','OrderController@again_deliver');

        Route::post('relation_customer','OrderController@relation_customer');

        Route::post('phone_customer','OrderController@phone_customer');

        Route::post('relation_consignee','OrderController@relation_consignee');

        Route::post('phone_consignee','OrderController@phone_consignee');

        Route::post('void_order','OrderController@void_order');

    });



});